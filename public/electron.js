const { app, BrowserWindow } = require('electron');
const isDev = require('electron-is-dev')
const path = require('path')
const http = require('http')
const si = require('systeminformation')

http.createServer(async (req, res) => {
    res.writeHead(200, {
        'Content-Type': 'text/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET',
        'Access-Control-Max-Age': 2592000, // 30 days
    });

    const cpu = await si.cpu() 
    const mem = await si.mem() 
    const fs = await si.diskLayout()
    const graphics = await si.graphics()

    console.log(graphics)
    const display = graphics.displays.find(display => display.connection.toLowerCase() === "internal")

    // console.log(await si.graphics())
    console.log(deviceScreen)

    res.end(JSON.stringify({
        processor_core_count:       cpu.cores,
        processor_frequency_mhz:    cpu.speed * 1000,
        memory_mb:                  mem.total / 1000000,
        storage_capacity_mb: fs.reduce((a, b) => a + b.size, 0) * 1000, // TODO: Figure out if correct
        storage_type: fs.some(disk => disk.type === "SSD") ? "ssd" : "hdd",
        display_size_inch: Math.sqrt( Math.pow(display.sizeX, 2) + Math.pow(display.sizeY, 2) ) * 0.03937008,
        touchscreen: deviceScreen.touchSupport,
        // pen,
        // usb_c,
        thumbnail_url: "https://cdn4.iconfinder.com/data/icons/planner-basic/64/writing-on-laptop-512.png",
        // noteb_name,
        // min_price,
        // max_price
    }));

}).listen(8260); 

app.on('ready', () => {
    let mainWindow = new BrowserWindow({
        width:800,
        height:600,
        show: false,
        frame: false,
        titleBarOverlay: true
    });
    
    mainWindow.once('ready-to-show', () => mainWindow.show());
    mainWindow.on('closed', () => {
        mainWindow = null;
    });

    mainWindow.loadURL(
        isDev
        ? "http://localhost:3000"
        : `file://${path.join(__dirname, "../build/index.html")}`
    );

    const { screen } = require('electron')
    deviceScreen = screen.getPrimaryDisplay()
});