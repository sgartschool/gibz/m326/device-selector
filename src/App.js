import React from "react";
import TabCompatibility, { reloadProfession } from "./tabs/Tab_Compatibility" 
import TabRecommendations from "./tabs/Tab_Recommendations" 
import {  Button, ButtonGroup, Box, Select } from "@chakra-ui/react"
import { CloseIcon } from '@chakra-ui/icons'
import { ColorModeScript } from "@chakra-ui/react"

import sampleSpecs from './sample-spec.json'

export default class App extends React.Component {
  state = {
    currentlyActiveTab: 0,
    currentlyActiveProfession: 0,
    currentlyActiveNotebook: localStorage.getItem("notebook") || 5179
  }

  constructor() {
    super()
    this.changeNotebook.bind(this)
    this.changeNotebookAndGoToMainTab.bind(this)
  }

  changeNotebook = (notebookID) => {
    this.setState({ currentlyActiveNotebook: notebookID })
    localStorage.setItem("notebook", notebookID)
    console.info('updatedNotebookID', this.state.currentlyActiveNotebook)
  }

  changeNotebookAndGoToMainTab = notebookID => {
    this.changeNotebook(notebookID)
    this.setState({ currentlyActiveTab: 0 })
  }

  render() {
    <ColorModeScript initialColorMode={"dark"} />
    return <Box display="flex" maxH="100vh" flexDirection="column">
      <Box pl={14} p={2} display="flex"
      backgroundColor={navigator.userAgent.includes("Electron") && "gray.100"}
      boxShadow={navigator.userAgent.includes("Electron") && "base"}>

        { navigator.userAgent.includes("Electron") &&
          <Button variant="ghost" mr={2} onClick={() => window.close()}>
            <CloseIcon/>
          </Button>
        } 
      
        <Select w={400} onChange={e => { this.setState({ currentlyActiveProfession: e.target.value }); reloadProfession( e.target.value ); this.setState({ currentlyActiveProfession: e.target.value }) }}>
          {sampleSpecs.map((spec, i) => <option key={i} value={i}>{spec.vocation_title}</option>)}
        </Select>

        <Box id="draggable" flexGrow="1" />

        <ButtonGroup spacing="2" colorScheme="purple" variant="outline">
          <Button
            colorScheme={this.state.currentlyActiveTab == 0 ? "purple" : null}
            onClick={() => this.setState({ currentlyActiveTab: 0 })}
          >Compatibility Check</Button>
          <Button
            colorScheme={this.state.currentlyActiveTab == 1 ? "purple" : null}
            onClick={() => this.setState({ currentlyActiveTab: 1 })}
          >Recommendations</Button>
        </ButtonGroup>

      </Box>


      <Box overflowY="scroll" pt={5} px={5}>
        {this.state.currentlyActiveTab == 0 &&
          <TabCompatibility
            key={this.state.currentlyActiveProfession + this.state.currentlyActiveNotebook}
            notebookID={this.state.currentlyActiveNotebook}
            changeNotebook={this.changeNotebook}
          />
        }
        {this.state.currentlyActiveTab == 1 &&
          <TabRecommendations
            key={this.state.currentlyActiveProfession}
            changeNotebook={this.changeNotebookAndGoToMainTab}
            profession={this.state.currentlyActiveProfession}
          />
        }
      </Box>
    </Box>
  }
}