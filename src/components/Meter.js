import { Progress, Box } from "@chakra-ui/react"
import { CheckCircleIcon, WarningTwoIcon } from "@chakra-ui/icons"

export default ({ progress, requiredPercentage, color }) => {
    const requiredMarkerPosition = 2 * requiredPercentage // in px

    return <Box w={200} sx={{ position: "relative" }}>
        <Progress colorScheme={color} value={progress} height="32px">test</Progress>
        {requiredPercentage
            ? <>
                <Box sx={{ left: `${requiredMarkerPosition}px`, background: "black", position: "absolute", top: "0px" }} height="32px" width="2px"></Box>
                { requiredMarkerPosition != 150 && 
                    <Box sx={{ left: `${requiredMarkerPosition-7}px`, position: "absolute", top: "27px" }} height="36px" width="2px"><WarningTwoIcon sx={{ color: "orange" }}/></Box>
                }
                <Box sx={{ left: `150px`, background: "black", position: "absolute", top: "0px" }} height="32px" width="2px"></Box>
                <Box sx={{ left: `143px`, position: "absolute", top: "26px" }} height="36px" width="2px"><CheckCircleIcon color={"green"} height={8}/></Box>
            </>
            : ""
        }
    </Box>
}