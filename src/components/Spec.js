import React from "react";
import { calculateComponentCompatibility } from "../services/compatibilityCalculatorService";
import { humanizeSpec, humanizeValue, dehumanizeValue } from "../services/humanizerService";
import { Input, Checkbox, Spacer, Flex, Box, Text, Tooltip, Select } from "@chakra-ui/react"
import Meter from '../components/Meter'

export default ({ spec, requiredData, recommendedData, inDeviceValue, callback }) => {
    const [ name, unit ] = humanizeSpec(spec)
    const requiredValue = requiredData[spec]
    const recommendedValue = recommendedData[spec]

    let [ actualValue, setActualValue ] = React.useState(inDeviceValue)
    
    const { percentage, requiredPercentage } = calculateComponentCompatibility(actualValue, requiredValue, recommendedValue, unit)

    const status = (actualValue < requiredValue || (unit == "bool" && !actualValue))
        ? "red" : (actualValue < recommendedValue)
            ? "orange" : "green"

    if (callback) callback(spec, unit == "bool" ? (actualValue ? 100 : 0) : percentage, status )

    const humanizationOfValue = value => {
        if (!value) return "doesn't matter"
        if (!unit)  return value
        if (unit == "bool")
            return `${value ? "yes" : "no"}`
        else
            return `${humanizeValue(value, spec, unit)} ${unit || ""}`
    }
  
    // TODO: Add centralized array for exceptions (like bool and storageType)
    return <Flex align="center" mb={5} key={name}>
      <Text w={150} mr={3}>{name}</Text>
      {unit == "storageType"
        ? <Select w={150} onChange={e => setActualValue(dehumanizeValue(e.target.value, spec, unit))}>
            {
              ["ssd", "hdd", "emmc"].map(opt => <option key={opt} value={opt} defaultValue={actualValue == opt}>{opt.toUpperCase()}</option>)
            }
        </Select>
        : <>
          {unit == "bool"
            ? <Checkbox defaultChecked={actualValue} onChange={e => setActualValue(e.target.checked * 2)}></Checkbox> // multiplied by two, in order to fill the entire bar
            : <Input w={150} defaultValue={humanizeValue(actualValue, spec, unit)} onChange={e => setActualValue(dehumanizeValue(e.target.value, spec, unit))}/>
          }
          {unit && unit != "bool" && unit != "storageType" &&
            <Text w={20} ml={3}>{unit}</Text>
          }
        </>
      }
  
      <Spacer/>
  
      <Tooltip label={`required: ${humanizationOfValue(requiredValue)}, recommended: ${humanizationOfValue(recommendedValue)}`}>
        <Box>
        <Meter progress={percentage} color={status} requiredPercentage={requiredPercentage} />
        </Box>
      </Tooltip>
    </Flex>
  }