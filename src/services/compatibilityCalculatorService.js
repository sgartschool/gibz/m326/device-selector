export const calculateComponentCompatibility = (actualValue, requiredValue, recommendedValue, unit) => {
    if (unit == "bool")
        return { percentage: 100 }

    // TODO: Remove type check
    if (typeof requiredValue == "string" || typeof recommendedValue == "string") {
        if (recommendedValue == actualValue)
            return { percentage: 100 }
        if (!requiredValue)
            return { percentage: 74 }
        else
            return { percentage: 0 }
    }

    // Calculate compatibility percentage
    let tempPercentage = 75 / recommendedValue * actualValue;
    // Return percentage if between -1 and 101, else return 100 or 0 depending on percentage
    let percentage = tempPercentage > 100
                        ? 100
                        : tempPercentage

    // Calculate percentage of the required values
    let tempRequiredPercentage = 75 / recommendedValue * requiredValue;
    // Return percentage if between -1 and 101, else return 100 or 0 depending on percentage
    let requiredPercentage = tempRequiredPercentage > 100 ? 100 : tempRequiredPercentage < 0 ? 0 : tempRequiredPercentage

    return {
        percentage: percentage,
        requiredPercentage: requiredPercentage
    }
}

export const calculateTotalCompatibility = globalSpecsArray => {
    const percentage = ([...globalSpecsArray.values()]
        .reduce((a, b) => a + b.percentage, 0) / globalSpecsArray.size)
        .toFixed()

    const red = [...globalSpecsArray.values()].filter(v => v.status === "red").length
    if (red)
        return { percentage, status: "red" }
    const orange = [...globalSpecsArray.values()].filter(v => v.status === "orange").length
    if (orange)
        return { percentage, status: "orange" }
    return { percentage, status: "green" }
}